import { Layout, Menu } from "antd";
import Head from "next/head";
import Link from "next/link";
import "antd/dist/antd.css";
import React from "react";
import { useRouter } from "next/router";
import { FixedHeader, MyContent } from "../shared/global.styled";

const { Footer } = Layout;

declare interface ILayoutProps {
  children: any;
  heads?: any;
}

export default function MyLayout({ children, heads }: ILayoutProps) {
  const router = useRouter();
  return (
    <React.Fragment>
      <Head>
        <title>Create Next App</title>
        <link rel="icon" href="/favicon.ico" />
        <meta
          name="viewport"
          content="width=device-width, initial-scale=1.0"
        ></meta>
        {heads ? heads : null}
      </Head>
      <Layout>
        <FixedHeader>
          <Menu
            theme="dark"
            mode="horizontal"
            defaultSelectedKeys={[router.pathname]}
          >
            <Menu.Item key="/">
              <Link href="/">
                <a>Home</a>
              </Link>
            </Menu.Item>
            <Menu.Item key="/news">
              <Link href="/news">
                <a>Soccer news</a>
              </Link>
            </Menu.Item>
            <Menu.Item key="/about">
              <Link href="/about">
                <a>About us</a>
              </Link>
            </Menu.Item>
          </Menu>
        </FixedHeader>
        <MyContent>{children}</MyContent>
        <Footer>NextJs by hieundm @ {new Date().getFullYear()}</Footer>
      </Layout>
    </React.Fragment>
  );
}
