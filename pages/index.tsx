import dynamic from "next/dynamic";

const DynamicHomeComponent = dynamic(
  () => import("./home"),
  {
    ssr: false
  }
);

export default function Home() {
  return (
    <DynamicHomeComponent></DynamicHomeComponent>
  )
}
