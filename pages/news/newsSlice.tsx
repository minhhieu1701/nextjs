import { createSlice } from "@reduxjs/toolkit";
import produce from "immer";

const initialState = {
  news: [],
};

export const actions = {
  GET_LIST: "GET_LIST",
  SET: "SET",
};

const newsSlice = createSlice({
  name: "news",
  initialState,
  reducers: {
    getList(state) {
      return produce(state, (draft) => {});
    },
    set(state, action) {
      state = produce(state, (draft) => {
        state.news = action.payload;
      });
    },
  },
});

export const { getList, set } = newsSlice.actions;

export const sagaKeys = {
  GET_LIST: getList.type,
  SET: set.type,
};

export default newsSlice.reducer;
