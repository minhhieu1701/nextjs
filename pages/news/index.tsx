import dynamic from "next/dynamic";
import React, { useEffect } from "react";
import MyLayout from "../../templates/layout";
import { connect } from "react-redux";
import { getList } from "./newsSlice";

const Container = dynamic(
  () => import("../../shared/global.styled").then((com) => com.Container),
  {
    ssr: false,
  }
);

const VideoList = dynamic(() => import("../../organisms/video-list"), {
  ssr: false,
});

function Index({ data, doGetList }) {
  useEffect(() => {
    doGetList();
  }, []);

  return (
    <MyLayout heads={<title>Soccer news</title>}>
      <Container>
        <VideoList listVideo={data}></VideoList>
      </Container>
    </MyLayout>
  );
}

const mapStateToProps = (_state: any) => {
  return {
    data: _state.newsReducer.news,
  };
};

const mapDispatchToProps = (
  dispatch: (arg0: { payload: undefined; type: string }) => any
) => ({
  doGetList: () => dispatch(getList()),
});

export default connect(mapStateToProps, mapDispatchToProps)(Index);
