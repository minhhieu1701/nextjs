import React from "react";
import App from "next/app";
import { Provider } from "react-redux";
import globalStore from "../store";

class MyApp extends App {
  static async getInitialProps({ Component, ctx }) {
    let pageProps = {};

    if (Component.getInitialProps) {
      pageProps = await Component.getInitialProps(ctx);
    }

    return { pageProps };
  }

  render() {
    const { Component, pageProps } = this.props;

    return (
      <Provider store={globalStore}>
        <Component {...pageProps} />
      </Provider>
    );
  }
}

export default MyApp;
