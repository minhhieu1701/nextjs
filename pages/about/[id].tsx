import { useRouter } from 'next/router'

export default function Test({ data }) {
    const router = useRouter();

    return (
        <h3>{router.query.id}</h3>
    );
}