import MyLayout from "../../templates/layout";

function About() {
    return (
        <MyLayout>
            This is about us.
        </MyLayout>
    );
}

export default About;