import { call, put } from "redux-saga/effects";
import { sagaKeys } from "../pages/news/newsSlice";
import axios from "axios";

export function* getNewsList() {
  const resp = yield call(fecthNewsList);

  yield put({ type: sagaKeys.SET, payload: resp });
}

/**
 *
 * Get news list
 * @return list video of news
 */
const fecthNewsList = async () => {
  const sUri = "https://www.scorebat.com/video-api/v1/";

  const resp = await axios.get(sUri);

  if (resp) {
    const aVideo = [];

    resp.data.map((x: { title: any; thumbnail: any, url: any }) => {
      aVideo.push({
        alt: x.title,
        coverUrl: x.thumbnail,
        description: `Description ${x.title}`,
        title: x.title,
        url: x.url,
      });
    });

    return aVideo;
  }

  return [];
};
