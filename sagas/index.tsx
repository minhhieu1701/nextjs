import { takeEvery } from "redux-saga/effects";
import { sagaKeys } from "../pages/news/newsSlice";
import { getNewsList } from "./news";

export function* rootSaga() {
  yield takeEvery(sagaKeys.GET_LIST, getNewsList);
}