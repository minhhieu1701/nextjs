import styled from "styled-components";
import { Layout } from "antd";

const { Header, Content } = Layout;

export const Container = styled.div`
  max-width: 100%;
  width: 100%;
  padding: 45px;
  @media (max-width: 768px) {
    padding: 15px;
  }
`;

export const FixedHeader = styled(Header)`
  position: fixed;
  top: 0;
  left: 0;
  width: 100%;
  z-index: 1000;
`;

export const MyContent = styled(Content)`
  padding-top: 64px;
`;
