import Video from "../../models/video";
import VideoBox from "../../molecules/video-box";
import { Row, Col } from "antd";
import React from "react";

declare interface IVideoListProps {
  listVideo?: Array<Video> | null;
}

export default function VideoList(props: IVideoListProps) {
  const [source, setSource] = React.useState(new Array<Video>());

  React.useEffect(() => {
    // console.log('-------------------------');
    // console.log(props.listVideo);
    setSource(props.listVideo);
    // console.log('-------------------------');
    // console.log('-------------------------');
    // console.log(source);
  }, [props]);

  return (
    <Row gutter={[16, 16]}>
      {source !== null && source !== undefined && source.length > 0 ? (
        source.map((x: Video, index: number) => {
          return (
            <Col key={index} xs={{ span: 24 }} md={{ span: 6 }}>
              <VideoBox
                alt={x.alt || ""}
                coverUrl={x.coverUrl || ""}
                description={x.description || ""}
                title={x.title}
                url={x.url}
              ></VideoBox>
            </Col>
          );
        })
      ) : (
        <h1>Khong co du lieu</h1>
      )}
    </Row>
  );
}
