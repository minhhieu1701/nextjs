export default class Video {
  alt: string | null;
  coverUrl: string | null;
  description: string | null;
  title: string;
  url: string;

  constructor(
    _alt: string | null,
    _coverUrl: string | null,
    _description: string | null,
    _title: string,
    _url: string
  ) {
    if (_alt) {
      this.alt = _alt;
    }

    if (_coverUrl) {
      this.coverUrl = _coverUrl;
    }

    if (_description) {
      this.description = _description;
    }

    this.title = _title;
    this.url = _url;
  }
}
