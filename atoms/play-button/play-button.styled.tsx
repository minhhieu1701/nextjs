import styled from "styled-components";

export const PlayButton = styled.button`
  background-color: transparent;
  border: none;
  box-shadow: none;
  position: relative;
  ::after {
    content: "";
    display: block;
    transition: all ease-in-out 0.3s;
    position: absolute;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);
    width: 100%;
    height: 100%;
  }
`;
