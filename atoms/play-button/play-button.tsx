import { CaretRightFilled } from "@ant-design/icons";
import * as Styled from "./play-button.styled";

declare interface IPlayButtonProps {
  className: string;
  onClick: Function;
}

export default function PlayButton(props: IPlayButtonProps) {
  return (
    <Styled.PlayButton className={props.className || ""}>
      <CaretRightFilled />
    </Styled.PlayButton>
  );
}
