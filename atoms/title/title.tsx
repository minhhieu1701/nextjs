import PropTypes from "prop-types";
import styledComponent from "./title.styled";

export default function Title({ text }) {
  return <styledComponent.styledTitle>{text}</styledComponent.styledTitle>;
}

Title.propTypes = {
  text: PropTypes.string.isRequired,
};
