import { Card } from "antd";

declare interface IVideoBoxProps {
  alt?: string;
  coverUrl?: string;
  description?: string;
  title?: string;
  url: string;
}

export default function VideoBox(props: IVideoBoxProps) {
  const onClick = (e) => {
    window.open(props.url);
  };

  return (
    <Card
      hoverable
      onClick={(event) => onClick(event)}
      cover={
        <img
          alt={props.alt || "test"}
          src={
            props.coverUrl ||
            "https://os.alipayobjects.com/rmsportal/QBnOOoLaAfKPirc.png"
          }
        />
      }
    >
      <Card.Meta
        title={props.title || "Title"}
        description={props.description || "Description"}
      />
    </Card>
  );
}
