import { combineReducers } from "redux";
import newsReducer from "./pages/news/newsSlice";

export default combineReducers({
  newsReducer,
});
